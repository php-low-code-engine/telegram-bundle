<?php

namespace PhpLowCodeEngine\TelegramBundle\Services\NodeType;

use App\Entity\Intergration;
use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Field\AutocomleteTextareaField;
use App\Services\Execution\ExecutionData;
use App\Services\Template\TemplateServiceInterface;
use App\Validator\Constraints\NodeParams;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use PhpLowCodeEngine\TelegramBundle\Services\Provider\TelegramProvider;
use PhpLowCodeEngine\TelegramBundle\TelegramException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class TelegramMessageNode implements \App\Services\NodeType\Types\NodeTypeInterface, \App\Services\Execution\ExecutionNodeInterface
{
    private ?Telegram $telegram = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly TemplateServiceInterface $templateService
    )
    {
    }

    static public function getName(): string
    {
        return 'Telegram message';
    }

    public function configureFields(Node $node): iterable
    {
        $integrationClass = TelegramProvider::class;
        $telegramIntegrations = $this->entityManager
            ->getRepository(Intergration::class)
            ->createQueryBuilder('i')
            ->select(['i.id', 'i.name'])
            ->where("i.type = '{$integrationClass}'")
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $choices = [];
        foreach ($telegramIntegrations as $telegramIntegration) {
            $choices[$telegramIntegration['name']] = $telegramIntegration['id'];
        }
        yield ChoiceField::new('integration')->setChoices($choices)->setValue($node->getParam('integration') ?? null);

        yield TextField::new('chat_id')->setValue($node->getParam('chat_id') ?? null);

        yield AutocomleteTextareaField::new('message')
            ->setValue($node->getParam('message') ?? null);
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validateParams() method.
    }

    public function run(NodeExecution $nodeExecution): string
    {
        $node = $nodeExecution->getNode();

        $chat_id = $this->templateService->render($node->getParam('chat_id'), $nodeExecution->getInputData());

        $message = $this->templateService->render($node->getParam('message'), $nodeExecution->getInputData());

        $telegram = $this->getTelegram($nodeExecution->getNode());
        Request::initialize($telegram);

        $this->logger->info('Send telegram message', [
            'message' => $message,
            'chat_id' => $chat_id,
            'service' => get_class($this->templateService) ,'method' => __METHOD__
        ]);

        Request::sendMessage([
            'chat_id' => $chat_id,
            'text'    => $message
        ]);

        return self::DEFAULT_RESULT;
    }

    private function getTelegram(Node $node): Telegram
    {
        if (!$this->telegram) {
            if (!$integrationId = $node->getParam('integration')) {
                throw new TelegramException('Not exists telegram integration yet');
            }

            $this->logger->debug("Search integration node '{$integrationId}'", ['method' => __METHOD__]);

            /** @var Intergration|TelegramProvider $integration */
            $integration = $this->entityManager->find(Intergration::class, $integrationId);

            if (!$integration) {
                throw new TelegramException("Not found integration '{$integrationId}'");
            }

            $this->logger->debug("Create Telegram by creds", ['creds' => $integration->getCreds(), 'method' => __METHOD__]);

            $this->telegram = new Telegram($integration->key);

        }

        return $this->telegram;
    }
}
