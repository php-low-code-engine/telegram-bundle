<?php

namespace PhpLowCodeEngine\TelegramBundle\Services\NodeType;

use App\Entity\Intergration;
use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionNodeInterface;
use App\Services\Execution\ExecutionService;
use App\Services\NodeType\NodeTypeFactoryService;
use App\Services\Trigger\Event\HttpRequestTriggerEvent;
use App\Services\Trigger\TriggerableNodeInterface;
use App\Services\Trigger\TriggerService;
use App\Validator\Constraints\NodeParams;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Telegram;
use PhpLowCodeEngine\TelegramBundle\Services\Provider\TelegramProvider;
use PhpLowCodeEngine\TelegramBundle\TelegramException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\EventDispatcher\Event;

class TelegramTriggerNode implements \App\Services\NodeType\Types\NodeTypeInterface,TriggerableNodeInterface, ExecutionNodeInterface
{
    private ?Telegram $telegram = null;

    public function __construct(
        private readonly ExecutionService $executionService,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger
    )
    {
    }

    static public function getName(): string
    {
        return 'TelegramTriggerNode';
    }

    public function configureFields(Node $node): iterable
    {
        $integrationClass = TelegramProvider::class;
        $telegramIntegrations = $this->entityManager
            ->getRepository(Intergration::class)
            ->createQueryBuilder('i')
            ->select(['i.id', 'i.name'])
            ->where("i.type = '{$integrationClass}'")
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $choices = [];
        foreach ($telegramIntegrations as $telegramIntegration) {
            $choices[$telegramIntegration['name']] = $telegramIntegration['id'];
        }

        yield ChoiceField::new('integration')->setChoices($choices)->setValue($node->getParam('integration') ?? null);

        yield TextField::new('webhook')->setValue($node->getParam('webhook') ?? null);

        foreach (Update::getUpdateTypes() as $updateType) {
            $optionsUpdates[$updateType] = $updateType;
        }
        yield ChoiceField::new('allowed_updates')
            ->setChoices($optionsUpdates)->allowMultipleChoices(true)
            ->setValue($node->getParam('allowed_updates') ?? null);
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validateParams() method.
    }

    /**
     * @param Node $node
     * @param Event|null $event
     * @return void
     * @throws TelegramException
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function addTrigger(Node $node, string $type)
    {
        if (in_array($type, [TriggerService::TYPE_REMOTE, TriggerService::TYPE_ALL])) {
            if (!$telegram = $this->getTelegram($node)) {
                return;
            }

            $webhookUrl = $this->getWebhookUrl($node);
            $allowUpdates = array_values($node->getParam('allowed_updates') ?? []);

            $this->logger->info("Set telegram webhook", ['webhook' => $webhookUrl, 'allowUpdates' => $allowUpdates, 'method' => __METHOD__]);

            $telegram->setWebhook($webhookUrl, $allowUpdates);
        }

        if (in_array($type, [TriggerService::TYPE_SYSTEM, TriggerService::TYPE_ALL])) {
            $this->logger->debug("Add listener 'http.request.event'", ['method' => __METHOD__]);
            $this->eventDispatcher->addListener('http.request.event', $this->getCallable($node));
        }
    }

    public function getWebhookUrl(Node $node): string
    {
        $webhookUrl = [
            getenv('HTTP_TRIGGER_BASE_URL'),
            getenv('HTTP_TRIGGER_BASE_FOLDER'),
            $node->getParam('webhook')
        ];

        return implode('/', $webhookUrl);
    }

    public function removeTrigger(Node $node, string $type)
    {
        if (in_array($type, [TriggerService::TYPE_REMOTE, TriggerService::TYPE_ALL])) {
            if (!$telegram = $this->getTelegram($node)) {
                return;
            }

            $webhookUrl = $this->getWebhookUrl($node);

            $this->logger->info("Delete telegram webhook", [
                'webhookUrl' => $webhookUrl,
                'method' => __METHOD__
            ]);

            $telegram->deleteWebhook();
        }

        if (in_array($type, [TriggerService::TYPE_SYSTEM, TriggerService::TYPE_ALL])) {
            $this->logger->debug("Remove listener 'http.request.event'", ['method' => __METHOD__]);
            $this->eventDispatcher->removeListener('http.request.event', $this->getCallable($node));
        }
    }

    public function run(NodeExecution $nodeExecution): string
    {
        $nodeExecution->setOutputData($nodeExecution->getInputData()->toArray());

        return self::DEFAULT_RESULT;
    }

    private function getTelegram(Node $node): ?Telegram
    {
        if (!$integrationId = $node->getParam('integration')) {
            $this->logger->warning("Not exists integration yet", ['params' => $node->getParams(), 'method' => __METHOD__]);
            return null;
        }

        if (!$this->telegram) {
            $this->logger->debug("Search integration node '{$integrationId}'", ['method' => __METHOD__]);

            /** @var Intergration|TelegramProvider $integration */
            $integration = $this->entityManager->find(Intergration::class, $integrationId);

            if (!$integration) {
                throw new TelegramException("Not found integration '{$integrationId}'");
            }

            $this->logger->debug("Create Telegram by creds", ['creds' => $integration->getCreds(), 'method' => __METHOD__]);

            $this->telegram = new Telegram($integration->key);
        }

        return $this->telegram;
    }

    public function getCallable(Node $node): \Closure
    {
        return function (HttpRequestTriggerEvent $event) use ($node) {
            $settingsUrl = $node->getParam('webhook');
            $dataRequest = $event->getRequest()->toArray();
            if ($settingsUrl === $event->getUrl()) {
                $this->logger->info("Triggered telegram node '{$node->getId()}'", [
                    'method' => __METHOD__,
                    'request' => $dataRequest
                ]);

                $execution = $this->executionService->createExecution($node->getWorkflow(), $node, $dataRequest);
                $this->executionService->runExecutionAsync($execution);

                $event->setResponse(new Response("Execution node '{$node->getId()}' started"));
            }
        };
    }

}
